//
//  Weather.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import Foundation

struct Weather : Codable {
    let description: String
    let icon: Int?
    let temperatureValue: Double?
    
    private enum RootKeys: String, CodingKey{
        case description = "WeatherText"
        case icon = "WeatherIcon"
        case temperature = "Temperature"
    }
    
    private enum TemperatureKeys: String, CodingKey{
        case metric = "Metric"
    }
    
    private enum MetricKeys: String, CodingKey{
        case value = "Value"
    }
    
    init(from decoder: Decoder) throws {
        let root = try decoder.container(keyedBy: RootKeys.self)
        self.description = try root.decode(String.self, forKey: .description)
        self.icon = try root.decode(Int.self, forKey: .icon)
        let temperature = try root.nestedContainer(keyedBy: TemperatureKeys.self, forKey: .temperature)
        let metric = try temperature.nestedContainer(keyedBy: MetricKeys.self, forKey: .metric)
        self.temperatureValue = try metric.decode(Double.self, forKey: .value)
    }
}
