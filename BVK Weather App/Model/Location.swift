//
//  Location.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import Foundation

struct Location: Codable {
    let key: String
    let cityName: String
    let countryName: String
    let position: GeoPosition
    
    
    private enum RootKeys: String, CodingKey {
        case key = "Key"
        case cityName = "EnglishName"
        case country = "Country"
        case position = "GeoPosition"
    }
    
    private enum CountryKeys: String, CodingKey {
        case name = "EnglishName"
    }
    
    init(from decoder: Decoder) throws {
        let root = try decoder.container(keyedBy: RootKeys.self)
        self.key = try root.decode(String.self, forKey: .key)
        self.cityName = try root.decode(String.self, forKey: .cityName)
        let country = try root.nestedContainer(keyedBy: CountryKeys.self, forKey: .country)
        self.countryName = try country.decode(String.self, forKey: .name)
        self.position = try root.decode(GeoPosition.self, forKey: .position)
    }
}

struct GeoPosition: Codable{
    var latitude: Double
    var longitude: Double
    
    private enum RootKeys: String, CodingKey{
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
    
    init(from decoder: Decoder) throws {
        let root = try decoder.container(keyedBy: RootKeys.self)
        self.latitude = try root.decode(Double.self, forKey: .latitude)
        self.longitude = try root.decode(Double.self, forKey: .longitude)
    }
    
    init(lat: Double = 0, lon: Double = 0) {
        
        self.latitude = lat
        self.longitude = lon
    }
}
