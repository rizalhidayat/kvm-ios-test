//
//  LocationManager.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import Foundation
import CoreLocation

class LocationManager{
    static let shared = LocationManager()
    
    let locationManager = CLLocationManager()
    
    func setupLocationManager(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func getCurrentPosition() -> GeoPosition {
        var position: GeoPosition = GeoPosition()
        let currentLocation = locationManager.location
        position.latitude = currentLocation?.coordinate.latitude ?? 0.0
        position.longitude = currentLocation?.coordinate.longitude ?? 0.0
        return position
    }
    
    
}
