//
//  AccuweatherAPI.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import Foundation

class AccuweatherAPI {
    static let shared = AccuweatherAPI()
    
    static let apiKey = "JQo7Ae7fiC4Wn2iYvMRav3NIVyRNzOIU"
    
    private init() {}
    
    func fetchSearchLocation(keyword: String, completion: @escaping ([Location]) -> ()){
        let stringURL = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey=\(AccuweatherAPI.apiKey)&q=\(keyword)"
        
        guard let url = URL(string: stringURL) else {
            return
        }
        
        URLSession.shared.dataTask(with: url){ (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {
                return
            }

            do {
                let decoder = JSONDecoder()
                let locations = try decoder.decode([Location].self, from: data)
                completion(locations)
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func fetchLocation(position: GeoPosition, completion: @escaping (Location) -> ()){
        let stringURL = "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=\(AccuweatherAPI.apiKey)&q=\(position.latitude)%2C\(position.longitude)"
        guard let url = URL(string: stringURL) else {
            return
        }
        
        URLSession.shared.dataTask(with: url){ (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            guard let data = data else {
                return
            }

            do {
                let decoder = JSONDecoder()
                let location = try decoder.decode(Location.self, from: data)
                completion(location)
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func fetchWeather(locationKey: String, completion: @escaping (Weather?) -> ()) {
        let stringURL = "http://dataservice.accuweather.com/currentconditions/v1/\(locationKey)?apikey=\(AccuweatherAPI.apiKey)"
        
        guard let url = URL(string: stringURL) else {
            completion(nil)
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            
            guard let data = data else {
                return
            }

            do {
                let decoder = JSONDecoder()
                let weathers = try decoder.decode([Weather].self, from: data)
                
                completion(weathers.first)
            } catch let error {
                completion(nil)
                print(error.localizedDescription)
            }
        }.resume()
        
    }
    
    
}

