//
//  SearchTableViewCell.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import UIKit

class LocationTableViewCell: UITableViewCell {
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var countryNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
