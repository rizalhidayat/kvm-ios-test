//
//  ViewController.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var degreeLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var weatherIV: UIImageView!
    
    let locationManager = LocationManager.shared
    var location: Location?
    var weather: Weather?
    
    var searchController: UISearchController!
    var resultController: ResultVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        locationManager.setupLocationManager()
        getCurrentPositionWeather()
        setupView()
        setupSearch()
    }
    
    func setupView(){
        locationLbl.text = location?.cityName
        degreeLbl.text = "\(weather?.temperatureValue ?? 0) °C"
        descriptionLbl.text = weather?.description
        
        let iconNum = weather?.icon ?? 0
        var iconString = ""
        iconString = (iconNum < 10) ? "0\(iconNum)" : "\(iconNum)"
        
        guard let url = URL(string: "http://apidev.accuweather.com/developers/Media/Default/WeatherIcons/\(iconString)-s.png") else { return  }

        DispatchQueue.global().async {
            guard let data = try? Data(contentsOf: url) else { return }
            DispatchQueue.main.async {
                self.weatherIV.image = UIImage(data: data)
            }
        }
    }
    
    func setupSearch(){
        resultController = self.storyboard?.instantiateViewController(withIdentifier: "ResultVC") as? ResultVC
        resultController.delegate = self
        searchController = UISearchController(searchResultsController: resultController)
        searchController.delegate = self
        searchController.searchResultsUpdater = self
        searchController.searchBar.autocapitalizationType = .none
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search Location"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    func getCondition(location: Location) {
        AccuweatherAPI.shared.fetchWeather(locationKey: location.key) { weather in
            self.weather = weather
            DispatchQueue.main.async {
                self.setupView()
            }
        }
    }
  
    func getCurrentPositionWeather(){
        let position = locationManager.getCurrentPosition()
        AccuweatherAPI.shared.fetchLocation(position: position) { location in
            self.location = location
            self.getCondition(location: location)
        }
    }
    
    @IBAction func onGetCurrentLocationClicked(_ sender: UIButton) {
        getCurrentPositionWeather()
    }
}

extension ViewController: UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        if let resultVC = searchController.searchResultsController as? ResultVC {
            if text != "" {
                resultVC.keyword = text
                resultVC.fetchSearchedLocations()
            }
            
        }
    }
    
}

protocol ResultProtocol {
    func didSelectedCell(location: Location)
}

extension ViewController: ResultProtocol{
    func didSelectedCell(location: Location) {
        self.location = location
        searchController.searchResultsController?.dismiss(animated: true, completion: nil)
        self.getCondition(location: location)
        searchController.searchBar.text = ""
    }
    
    
}
