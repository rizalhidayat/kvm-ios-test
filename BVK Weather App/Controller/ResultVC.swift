//
//  ResultVC.swift
//  BVK Weather App
//
//  Created by Rizal Hidayat on 15/11/21.
//

import UIKit

class ResultVC: UITableViewController {
    var locations = [Location]()
    var keyword = ""
    var delegate: ResultProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        tableView.dataSource = self
    }

    func fetchSearchedLocations() {
        AccuweatherAPI.shared.fetchSearchLocation(keyword: keyword) { locations in
            self.locations.removeAll()
            self.locations = locations
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return locations.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as? LocationTableViewCell
        else { return UITableViewCell()}

        cell.locationLbl.text = locations[indexPath.row].cityName
        cell.countryNameLbl.text = locations[indexPath.row].countryName
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let location = locations[indexPath.row]
        delegate?.didSelectedCell(location: location)
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
